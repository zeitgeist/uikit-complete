#   Complete UIKit package for Meteor
##  UIKit Version 2.22.0

Official UI Kit Documentation : http://getuikit.com/

Gitlab repo : https://gitlab.com/zeitgeist/uikit-complete.git

<a href="http://uikit-complete.meteor.com/">Sample page</a> that uses this package and the <a href="https://gitlab.com/zeitgeist/sample-uikit-complete-meteor">source code</a> for those who are curious. 

### How to install?
```
meteor add coffeejesus:uikit-complete
```

### How to uninstall?
```
meteor remove coffeejesus:uikit-complete
```

### Are you using iron router?
If you are, please add these lines on startup. Otherwise, components like sidebar, sticky, smooth scroll etc. will not work. More detail on this issue can be found <a href="https://gitlab.com/zeitgeist/uikit-complete/issues/1">here</a>.
```
Meteor.startup(function() {
    $('body').attr('data-uk-observe', '1');
});
```

### What are included in this package?

#### Components:

* accordion
* autocomplete
* datepicker
* dotnav
* form-password
* form-advanced
* form-file
* form-select
* grid
* htmleditor
* lightbox
* nestable
* notify
* pagination
* placeholder
* progress
* search
* slidenav
* slideshow-fx
* slideshow
* slider
* slideset
* sortable
* sticky
* tooltip
* upload

#### Core:
* alert
* button
* cover
* dropdown
* grid
* modal
* nav
* offcanvas
* parallax
* scrollspy
* smooth-scroll
* switcher
* tab
* toggle
* touch
* utility
* core
